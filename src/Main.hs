import Diagrams.Prelude
import Diagrams.Backend.Cairo.CmdLine
import Data.List
import Control.Arrow

lineColours :: [Colour Double]
lineColours = cycle [lightgreen, mediumorchid, lightblue, salmon]
-- lineColours = repeat firebrick

fieldColourWhite, fieldColourBlack, borderColour, wallColour, holeColour :: Colour Double
fieldColourWhite = white
fieldColourBlack = grey
borderColour = black
wallColour = firebrick
holeColour = black

roundChess :: Int -> Double -> Double -> Diagram B R2
roundChess ranks outer inner =
    colouredLines <>
    borders <>
    markers <>
    circle inner # fc holeColour # lw none <>
    board
  where
    colouredLines = mconcat $
        zipWith (\col bl -> bl # lc col # lw ultraThin) lineColours bishopLines

    bishopLines = take 24
        $ iterate (\b -> b # rotateBy (1/24))
        $ crossLine <> complexLine

    -- bishopLines = [crossLine <> complexLine]
    complexLine ::Diagram B R2
    complexLine = (cubicSpline False to <> cubicSpline False from)
        # rotateBy (1/48)

    to = map (\i -> startField
        # translateY (abs i * step)
        # rotateBy (i/24)) [0.0..(fromIntegral ranks - 1)]

    startField = p2 (0,0) # translateY (inner + 0.5*step)

    -- wat
    from = reverse $ map (\p -> p2 (second negate (unp2 p)) # rotateBy (-2/24)) to


    crossLine = fromVertices [middleInner, middleInner # rotate (10/24 @@ turn)]

    middleInner = p2 (0,0)
        # translateY (inner + 0.5*step)
        # rotateBy (1/48)

    borders =
        circle outer # lw ultraThin # lc borderColour <>
        circle inner # lw ultraThin # lc borderColour

    board = mconcat $
        map (\(o,i,b) -> segmentedCircle o i b) (radi ranks outer inner)

    markers = mconcat [
        marker,
        marker # rotateBy (1/3),
        marker # rotateBy (2/3)]

    marker = thinMarker <> fatMarker

    fatMarker = fromOffsets [r2 (0, step)]
        # lc wallColour
        # lw thick
        # translateY (outer - step)

    thinMarker = fromOffsets [r2 (0, step * 2)]
        # lc wallColour
        # translateY (outer - step * 3)

    step = (outer - inner) / fromIntegral ranks

segmentedCircle :: Double -> Double -> Bool -> Diagram B R2
segmentedCircle outer inner b = mconcat $ map angleToWedge (angles b)
  where
    angleToWedge (l, r, col) = annularWedge outer inner l r
        # fc (blackWhite col)
        # lw none

    blackWhite True = fieldColourWhite
    blackWhite False = fieldColourBlack

angles :: Bool -> [(Angle, Angle, Bool)]
angles startCol = take 24 $ unfoldr mkAngle (0,startCol)
  where
    mkAngle (cur, col) = Just (
        (cur/24 @@ turn , (cur+1)/24 @@ turn, col), -- current value
        (cur+1, not col)) -- new seed

radi :: Int -> Double -> Double -> [(Double, Double, Bool)]
radi n outer inner = take n $ unfoldr mkRadi (inner, False)
  where
    step = (outer - inner) / fromIntegral n
    mkRadi (cur, b) = Just (
        (cur, cur + step, b), -- current value
        (cur + step, not b)) -- new seed

main :: IO ()
main = mainWith $ roundChess 6 3 1
